package implementation;

//This is the concrete builder class that assembles the parts of the finished Laptop object

public class AsusPredator implements LaptopBuilder {

    private Laptop laptop;

    public AsusPredator() {

        this.laptop = new Laptop();

    }

    @Override
    public void buildScreenSize() {

        laptop.setScreenSize((float) 15.6);

    }

    @Override
    public void buildProcessor() {

        laptop.setProcessor("3.8Ghz Intel Core i7");

    }

    @Override
    public void buildMemory() {

        laptop.setMemory("256GB Flash Memory SSD");

    }

    @Override
    public void buildRAM() {

        laptop.setRAM("16GB DDR4 SDRAM");

    }

    @Override
    public void buildGraphicsCard() {

        laptop.setGraphicsCard("NVIDIA GeForce GTX 1060");

    }

    @Override
    public Laptop getLaptop() {

        return this.laptop;

    }


}