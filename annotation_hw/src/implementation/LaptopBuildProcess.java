package implementation;

import implementation.AsusPredator;
import implementation.Laptop;
import implementation.LaptopBuilder;

public class LaptopBuildProcess {

    public static void main(String[] args) {

        LaptopBuilder apple = new MacbookPro();
        LaptopBuilder microsoft = new SurfaceLaptop();
        LaptopBuilder asus = new AsusPredator();

        LaptopEngineer laptopEngineer1 = new LaptopEngineer(apple);
        LaptopEngineer laptopEngineer2 = new LaptopEngineer(microsoft);
        LaptopEngineer laptopEngineer3 = new LaptopEngineer(asus);

        laptopEngineer1.makeLaptop();
        laptopEngineer2.makeLaptop();
        laptopEngineer3.makeLaptop();

        Laptop macbookPro = laptopEngineer1.getLaptop();
        Laptop surfaceLaptop = laptopEngineer2.getLaptop();
        Laptop asusPredator = laptopEngineer3.getLaptop();

        macbookPro.showLaptop();
        surfaceLaptop.showLaptop();
        asusPredator.showLaptop();

    }

}
