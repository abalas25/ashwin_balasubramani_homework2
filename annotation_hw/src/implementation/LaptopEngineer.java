package implementation;//This engineer class creates a Laptop using the builder interface that is defined

import implementation.Laptop;
import implementation.LaptopBuilder;

public class LaptopEngineer {

    private LaptopBuilder laptopBuilder;

    //Each laptop's specification is sent to the LaptopEngineer

    public LaptopEngineer(LaptopBuilder laptopBuilder) {

        this.laptopBuilder = laptopBuilder;

    }

    //Return the Laptop made using the each laptops's specifications

    public Laptop getLaptop() {

        return this.laptopBuilder.getLaptop();

    }

    //Executes the methods specific to the LaptopBuilder that implements the each laptop

    public void makeLaptop() {

        this.laptopBuilder.buildScreenSize();

        this.laptopBuilder.buildProcessor();

        this.laptopBuilder.buildMemory();

        this.laptopBuilder.buildRAM();

        this.laptopBuilder.buildGraphicsCard();

    }

}