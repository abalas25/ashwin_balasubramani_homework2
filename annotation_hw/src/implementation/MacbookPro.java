package implementation;

//This is the concrete builder class that assembles the parts of the finished Laptop object

import implementation.Laptop;
import implementation.LaptopBuilder;

public class MacbookPro implements LaptopBuilder {

    private Laptop laptop;

    public MacbookPro() {

        this.laptop = new Laptop();

    }

    @Override
    public void buildScreenSize() {

        laptop.setScreenSize((float) 13.3);

    }

    @Override
    public void buildProcessor() {

        laptop.setProcessor("1.4GHz quad-core Intel Core i5");

    }

    @Override
    public void buildMemory() {

        laptop.setMemory("256GB SSD");

    }

    @Override
    public void buildRAM() {

        laptop.setRAM("8GB LPDDR3");

    }

    @Override
    public void buildGraphicsCard() {

        laptop.setGraphicsCard("Intel Iris Plus Graphics 645");

    }

    @Override
    public Laptop getLaptop() {

        return this.laptop;

    }

}
