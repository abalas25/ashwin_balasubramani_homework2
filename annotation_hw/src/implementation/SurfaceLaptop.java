package implementation;//This is the concrete builder class that assembles the parts of the finished Laptop object

import implementation.Laptop;
import implementation.LaptopBuilder;

public class SurfaceLaptop implements LaptopBuilder {

    private Laptop laptop;

    public SurfaceLaptop() {

        this.laptop = new Laptop();

    }

    @Override
    public void buildScreenSize() {

        laptop.setScreenSize((float) 13.5);

    }

    @Override
    public void buildProcessor() {

        laptop.setProcessor("Quad-core Intel Core i5-1035G7");

    }

    @Override
    public void buildMemory() {

        laptop.setMemory("256GB SSD");

    }

    @Override
    public void buildRAM() {

        laptop.setRAM("16GB LPDDR4x");

    }

    @Override
    public void buildGraphicsCard() {

        laptop.setGraphicsCard("Iris Plus 950");

    }

    @Override
    public Laptop getLaptop() {

        return this.laptop;

    }


}