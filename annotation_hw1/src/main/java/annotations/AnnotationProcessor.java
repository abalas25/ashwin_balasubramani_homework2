// This file is an annotation processor for the type METHOD

package annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class AnnotationProcessor {
    public static void main(String[] args) {
         System.out.println("Processing...");

         int success = 0, failed = 0, total = 0, disabled = 0;

         // Getting the annotations mentioned in the AnnotationTest.java class into an object for processing
         Class<AnnotationTest> processorObj = AnnotationTest.class;

         for(Method method : processorObj.getDeclaredMethods()) {
            // if method is annotated with @MethodAnnotation
            if(method.isAnnotationPresent(MethodAnnotation.class)) {
                Annotation annotation = method.getAnnotation(MethodAnnotation.class);
                MethodAnnotation customMethod = (MethodAnnotation) annotation;

                if(customMethod.present()){
                    String present = "n/a";

                    try {
                        present = (String) method.invoke(processorObj.newInstance());
                        System.out.printf("%s - customMethod '%s' - processed %n - present: %n",
                                ++total,
                                method.getName(),
                                present);

                        success++;

                    } catch(Throwable e) {
                        System.out.printf("%s - customMethod '%s' - didn't processed %n - present: %s %n",
                                ++total,
                                method.getName(),
                                e.getCause());

                        failed++;
                    }
                }
                else {
                    System.out.printf("%s - customMethod '%s' - didn't processed%n",
                            ++total,
                            method.getName());

                    disabled++;
                }
            }
        }
        System.out.printf("%nResult : Total : %d, Successful : %d, Failed : %d, Disabled : %d%n", total, success, failed, disabled);
    }
}
