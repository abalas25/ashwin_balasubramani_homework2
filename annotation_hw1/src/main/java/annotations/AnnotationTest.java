// This file has the inputs for all the custom annotations, that will be checked using an annotation processor.

package annotations;

@Input (
        screenSize = 13,
        processor = "Intel i5 Gen 8",
        memory = "256GB SSD",
        ram = "16GB LPDDR4",
        graphicsCard = "NVIDIA GTX1080 Titan"
)

public class AnnotationTest {
    @MethodAnnotation
    String alwaysPresent() {
        return "Always returns..";
    }

    @MethodAnnotation
    String willThrowException() {
        throw new RuntimeException("Throws a runtime exception..");
    }

    @MethodAnnotation
    String neverProcess() {
        throw new RuntimeException("Never processes..");
    }
}
