package lombok_implementation;// This class demonstrates the use of Lombok library to automatically produce getters, setters and builder boilerplate code

import lombok.*;

@Getter
@Setter
public class LombokLaptop  {

    private double screenSize;
    private String processor;
    private String memory;
    private String ram;
    private String graphicsCard;

}
