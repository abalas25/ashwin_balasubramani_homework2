package lombok_implementation;// This is an example of an annotation processor for custom annotation of TYPE element

import annotations.AnnotationTest;
import annotations.Input;

import java.lang.annotation.Annotation;

@Input
public class LombokLaptopBuilder {
    public static void main(String[] args) {
        // Getting the annotations mentioned in the AnnotationTest.java class into an object for processing
        Class<AnnotationTest> processorObj = AnnotationTest.class;

        // Process @Input
        if(processorObj.isAnnotationPresent(Input.class)) {
            Annotation annotation = processorObj.getAnnotation(Input.class);
            Input inputValues = (Input) annotation;

            System.out.println("Laptop build using custom annotation..");
            System.out.println("Screen Size: " + inputValues.screenSize());
            System.out.println("Processor: " + inputValues.processor());
            System.out.println("Memory: " + inputValues.memory());
            System.out.println("RAM: " + inputValues.ram());
            System.out.println("Graphics Card: " + inputValues.graphicsCard());
        }
    }
}
