import annotations.Input;
import org.testng.annotations.Test;

import java.lang.annotation.Annotation;

public class test1 {
    Annotation annotation = Test.class.getAnnotation(Input.class);
    if(annotation instanceof Input) {
        Input input = (Input) annotation;

        assertequals(13, info.screenSize());
        assertequals("Intel i5 Gen 8", info.processor());
        assertequals("256GB SSD", info.memory());
        assertequals("8GB LPDDR4", info.ram());
        assertequals("NVIDIA GTX1080 Titan", info.graphicsCard());
    }
    else {
        fail("Did not find the annotation");
    }


}
